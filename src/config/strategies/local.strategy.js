const passport = require('passport');
const { Strategy } = require('passport-local');
const debug = require('debug')('app:authLogin');
const { MongoClient } = require('mongodb');

const localStrategy = () => {
  passport.use(new Strategy({
    usernameField: 'auth[username]',
    passwordField: 'auth[password]'
  }, async (username, password, done) => {
    const url = 'mongodb://root:123456@mongo:27017';
    const dbName = 'library';
    let client;

    try {
      client = await MongoClient.connect(url);

      const db = client.db(dbName);
      const user = await db.collection('users').findOne({ username });

      if (user.password !== password) {
        return done(null, false);
      }

      return done(null, user);
    } catch (e) {
      debug(e.stack);
      return done(null, false);
    } finally {
      client.close();
    }
  }));
};

module.exports = localStrategy;
