// const sql = require('mssql');
const { MongoClient, ObjectID } = require('mongodb');
const debug = require('debug')('app:bookController');

const bookController = (bookService, nav) => {
  const middleware = (req, res, next) => {
    if (!req.user) {
      return res.redirect('/auth/signin');
    }

    next();
  };

  const getIndex = async (_, res) => {
    let client;
    try {
      const url = 'mongodb://root:123456@mongo:27017';
      const dbName = 'library';

      client = await MongoClient.connect(url);

      const db = client.db(dbName);
      const books = await db.collection('books').find().toArray();

      debug(books);

      res.render('books/books', {
        nav,
        title: 'My books',
        books
      });
    } catch (e) {
      debug(e.stack);
      res.render(`Error: ${e}`);
    } finally {
      client.close();
    }
  };

  const getById = async (req, res) => {
    let client;
    try {
      const { id } = req.params;
      const url = 'mongodb://root:123456@mongo:27017';
      const dbName = 'library';

      client = await MongoClient.connect(url);
      debug(id);
      const db = client.db(dbName);
      const book = await db.collection('books').findOne({ _id: new ObjectID(id) });

      debug(book);

      if (!book) {
        debug('Book don\'t find');
        res.status(404).redirect('/');
        return;
      }

      book.details = await bookService.getBookById(book.bookId || Math.floor(Math.random() * 100));

      debug(book.details);

      res.render('books/book', {
        nav,
        book
      });
    } catch (e) {
      debug(e.stack);
      res.status(404).redirect('/');
    }
  };

  return {
    getIndex,
    getById,
    middleware
  };
};

module.exports = bookController;
