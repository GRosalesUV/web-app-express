const express = require('express');
const { MongoClient } = require('mongodb');
const debug = require('debug')('app:authRoutes');
const passport = require('passport');

const authRoutes = express.Router();

const router = (nav) => {
  authRoutes.route('/signIn')
    .get(async (req, res) => {
      res.render('signin', {
        nav,
        title: 'Sign In'
      });
    })
    .post(passport.authenticate('local', {
      successRedirect: '/books',
      failureRedirect: '/auth/signin'
    }));

  authRoutes.route('/signUp')
    .post(async (req, res) => {
      const { username, password } = req.body.auth;
      let client;
      try {
        const url = 'mongodb://root:123456@mongo:27017';
        const dbName = 'library';

        client = await MongoClient.connect(url);

        const db = client.db(dbName);

        const collection = db.collection('users');
        const user = (await collection.insertOne({ username, password })).ops[0];
        debug(user);

        // Login user
        req.login(user, () => {
          res.redirect('/auth/profile');
        });
      } catch (e) {
        debug(e.stack);
        res.status(500).send('Server Error');
      } finally {
        client.close();
      }
    });

  authRoutes.route('/profile')
    .all((req, res, next) => {
      if (!req.user) {
        res.redirect('/auth/signin');
      }

      next();
    })
    .get(async (req, res) => {
      res.json(req.user);
    });

  return authRoutes;
};

module.exports = router;
