const debug = require('debug')('app:Service.goodreads');
const axios = require('axios');

const bookService = () => {
  const getBookById = (id) => (
    new Promise((resolve, reject) => {
      axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then((response) => {
          resolve({ description: response.data.body });
        })
        .catch((err) => {
          debug(err);
          reject(err);
        });
    })
  );

  return {
    getBookById
  };
};

module.exports = bookService();
